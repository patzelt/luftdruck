Physische Geographie kompakt

Physische Geographie kompakt
© 2010
Soft cover - Print (ISBN: 978-3-662-50460-4)
Electronic (ISBN: 978-3-662-50461-1)


Der Luftdruck bestimmt im Wesentlichen die Wind-
und Bewölkungsverhältnisse und hat damit Einfluss auf
alle weiteren Klimaelemente. Dies ist der Grund für
seine zentrale Stellung in der Wettervorhersage sowie bei
der Beschreibung und dem Verständnis von einzelnen
Wetterabläufen. In der langfristigen, klimatologischen
Betrachtung sind die wichtigsten Klimaelemente jedoch
Lufttemperatur und Niederschlag.
Der Luftdruck ist das Gewicht der Luftsäule, die sich
jeweils oberhalb eines bestimmten Ortes findet. Die
temperaturabhängige Luftdruckabnahme mit der Höhe
wird durch die barometrische Höhenformel beschrie-
ben. Ausgehend vom mittleren Luftdruck im Meeres -
niveau (1 018 hPa) ergibt sich daraus die Kurve des
höhenabhängigen Luftdrucks (Abb. 5.4). Die Abbildung
zeigt, dass in einer Höhe von ca. 2 200 m noch 75% des
Ausgangsluftdrucks herrschen. Zu 50% und 25% des
Ausgangsluftdrucks gehören Höhen von etwa 5 100
beziehungsweise 9 000 m. Das bedeutet, dass in den
untersten 9 000 m der Atmosphäre etwa drei Viertel der
gesamten Luftmasse konzentriert sind.
Für Überschlagsrechnungen zur Temperaturabhän-
gigkeit der Luftdruckabnahme lässt sich daraus eine ein-
fache Regel ableiten. Der vertikale Abstand von zwei
Luftdruckniveaus nimmt pro Grad Temperaturerhö-
hung um vier Tausendstel zu. Das heißt, die isobaren
Flächen (Flächen gleichen Luftdrucks) haben in warmer
Luft einen größeren Abstand als in kalter Luft (Abb. 5.5).
Die daraus in unterschiedlichen Höhen auftretenden
Druckgradienten bedingen die Entstehung von Wind
und unterschiedlichen Wetterlagen.
Bei ausgeprägten Hochdruckwetterlagen können
großräumige Luftströmungen in einem bestimmten
Gebiet eine untergeordnete Rolle spielen. Die Werte der
meisten Klimaelemente sind eng an die Sonnenstrah-
lung gekoppelt und zeigen deshalb einen ausgeprägten
Tagesgang. Man spricht in solchen Fällen von autoch-
thonen Wetterlagen. Bei allochthonen Wetterlagen
dagegen werden die Werte der Klimaelemente vor allem
dadurch bestimmt, dass über den Wind Luftmassen mit
bestimmten Eigenschaften (Temperatur, Luftfeuchte,
Bewölkung etc.) in eine Region geführt werden.

