# Luftdruck

Vortrag für den Kurs "Einführung in die Klima- und Hydrogeographie"

um das ganze Projekt also Vortrag und Quelldateien mit git zu kopieren folgenden Befehl ausführen:
```
git clone https://git.imp.fu-berlin.de/patzelt/luftdruck.git
```

Das Bild von einem Dosenbaometer habe ich selbst aufgenommen und veröffentliche es unter Creative Commons Lizens CC BY-SA
![Dosenbaometer: rechts eine Dose welche einen Schreiber bewegt welcher auf eine Walze mit Papier die Luftdruckwerte schreibt.](dosenbaometer.jpeg)