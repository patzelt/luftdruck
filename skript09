https://www.studocu.com/de/document/freie-universitat-berlin/einfuhrung-in-die-klima-und-hydrogeographie/skript/474799

Der Luftdruck bezeichnet die Kraft die senkrecht von der Oberschicht der 
Erdatmosphäre auf die Erdoberfläche wirkt.  
Der mittlere Luftdruck auf Meeresniveau entspricht ungefähr 101.300 N/m² = 
1013 hPa. An der Tropopause herrscht lediglich noch ein Luftdruck von ~100 
hPa. 
 
3.5.3  Änderung des Luftdrucks mit der Höhe 
 
Der Luftdruck sinkt mit abnehmender Höhe, da sich die auflastende Luftsäule 
verkürzt. Dabei sinkt neben dem Luftdruck selbst aufgrund der abnehmenden 
Dichte ebenso der Druckgradient einzelner Schichten. 
 
Die Abnahme des Luftdrucks ist in erster Linie von der Luftdichte sowie der 
Änderung der Höhe abhängig. 
 
3.5.4 Hydrostatische Grundgleichung 
 
(1)  dp g dz
ρ
=− ⋅ ⋅  
Mit 
 dp     – Änderung des Luftdrucks 
 g     – Schwerebeschleunigung 
 ρ (rho)   – Luftdichte 
 dz   – Änderung der Höhe 
 
Æ dp ~ dz (Proportionalität Druck- zu Höhenänderung) 
 
Es gilt: 
(2) 
p
RT
ρ
=⋅ 
Mit 
 ρ (rho)   – Luftdichte 
p     – Luftdruck 
 R     – allg. Gaskonstante 
 T     – absolute Temperatur für die betrachtete Luftschicht 
 
3.5.5 Barometrische Höhenformel 
 
Aus (1) und (2) (s.o.) folgt die barometrische Höhenformel, die neben der 
Proportionalität von Druck- und Höhenänderung ebenfalls eine umgekehrte 
Proportionalität zur Lufttemperatur: 
 
(3) 
p
dp g dz
RT
=− ⋅ ⋅
⋅ 
Mit 
dp  – Änderung des Luftdrucks 
 g   – Schwerebeschleunigung 
 dz   – Änderung der Höhe 
p   – Luftdruck 
 10

